package ro.jzp.bibliereader.activity;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;

import org.json.JSONException;

import java.io.IOException;

import ro.jzp.bibliereader.R;
import ro.jzp.bibliereader.adapter.CardDailyViewAdapter;
import ro.jzp.bibliereader.entity.Plan;
import ro.jzp.bibliereader.repository.PlanRepository;

public class DailyActivity extends AppCompatActivity {
    private ViewPager mPager;

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_daily, menu);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_settings:
                Intent intent = new Intent(this, SettingsActivity.class);

                startActivity(intent);

                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_daily);

        mPager = (ViewPager) findViewById(R.id.card_daily_reference_pager);

        DisplayDailyView drawer = new DisplayDailyView();
        drawer.execute(this);
    }

    private class DisplayDailyView extends AsyncTask<DailyActivity, Void, CardDailyViewAdapter> {
        private DailyActivity context;
        private Plan plan;

        @Override
        protected CardDailyViewAdapter doInBackground(DailyActivity... params) {
            this.context = params[0];

            try {
                plan = PlanRepository.getInstance().fetch();
            } catch (IOException | JSONException e) {
                System.out.println(e);
                plan = new Plan();
            }

            return new CardDailyViewAdapter(
                this.context.getSupportFragmentManager(),
                plan
            );
        }

        protected void onPostExecute(CardDailyViewAdapter adapter) {
            super.onPostExecute(adapter);

            this.context.mPager.setAdapter(adapter);

            mPager.setCurrentItem(plan.getPositionByCurrentDay());
        }
    }
}
