package ro.jzp.bibliereader.activity;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.view.View;
import android.widget.ExpandableListView;
import org.json.JSONException;
import java.io.IOException;
import ro.jzp.bibliereader.fragment.DailyActivityFragment;
import ro.jzp.bibliereader.adapter.PlanListingAdapter;
import ro.jzp.bibliereader.R;
import ro.jzp.bibliereader.entity.Plan;
import ro.jzp.bibliereader.entity.PlanItem;
import ro.jzp.bibliereader.helper.Helper;
import ro.jzp.bibliereader.repository.PlanRepository;

public class PlanListingActivity extends AppCompatActivity {
    private Plan plan;

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                super.onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        try {
            plan = PlanRepository.getInstance().fetch();
        } catch (IOException | JSONException e) {
            plan = new Plan();
        }

        getSupportActionBar().setTitle("Un legamânt nou");
        getSupportActionBar().setSubtitle("Un mod de viață transformat");
        getSupportActionBar().setHomeButtonEnabled(true);

        ExpandableListView expandableListView = (ExpandableListView) findViewById(R.id.plan_listing);
        expandableListView.setAdapter(new PlanListingAdapter(this, plan));
        expandableListView.setSelection(getIntent()
            .getIntExtra(DailyActivityFragment.CURRENT_POSITION_EXTRA, 0)
        );
        expandableListView
            .setOnChildClickListener(new ExpandableListView.OnChildClickListener() {
                @Override
                public boolean onChildClick(
                    ExpandableListView parent,
                    View v,
                    int groupPosition,
                    int childPosition,
                    long id
                ) {
                    PlanItem item = plan
                        .offsetGet(groupPosition).get(childPosition);

                    startActivity(Helper.generateBibleURLIntent(
                        item,
                        getApplicationContext()
                    ));

                    return false;
                }
            });
    }
}
