package ro.jzp.bibliereader.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

import ro.jzp.bibliereader.R;
import ro.jzp.bibliereader.entity.PlanItem;

public class CardDailyReferenceListAdapter extends ArrayAdapter {
    private List<PlanItem> list;
    private Context context;

    public CardDailyReferenceListAdapter(Context context, List list) {
        super(context, R.layout.card_daily_reference_list_item, list);
        this.list = list;
        this.context = context;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return this.list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) this.context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

            convertView = inflater
                .inflate(R.layout.card_daily_reference_list_item, null);
        }

        TextView text = (TextView) convertView
            .findViewById(R.id.card_daily_list_item);

        text.setText(list.get(position).getLabel());

        return convertView;
    }
}
