package ro.jzp.bibliereader.adapter;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import ro.jzp.bibliereader.entity.Plan;
import ro.jzp.bibliereader.fragment.DailyActivityFragment;

public class CardDailyViewAdapter extends FragmentStatePagerAdapter {
    public static final String CURRENT_POSITION = "current_position";
    private Plan plan;

    public CardDailyViewAdapter(
        FragmentManager fm,
        Plan plan
    ) {
        super(fm);

        this.plan = plan;
    }

    @Override
    public Fragment getItem(int position) {
        Fragment view = new DailyActivityFragment();
        Bundle params = new Bundle();
        params.putInt(CURRENT_POSITION, position);
        view.setArguments(params);

        return view;
    }

    @Override
    public int getCount() {
        return plan.getSize();
    }
}