package ro.jzp.bibliereader.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.TextView;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import ro.jzp.bibliereader.R;
import ro.jzp.bibliereader.entity.Plan;
import ro.jzp.bibliereader.entity.PlanItem;
import ro.jzp.bibliereader.helper.DateHelper;

public class PlanListingAdapter extends BaseExpandableListAdapter {
    private LayoutInflater mInflater;

    private Plan plan;

    public PlanListingAdapter(
        Context context,
        Plan plan
    ) {
        this.plan = plan;
        mInflater = (LayoutInflater)context
            .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getGroupTypeCount() {
        return 2;
    }

    @Override
    public Object getChild(int groupPosition, int itemPosition) {
        return plan.offsetGet(groupPosition).get(itemPosition);
    }

    @Override
    public long getChildId(int groupPosition, int itemPosition) {
        return itemPosition;
    }

    @Override
    public View getChildView(
        int groupPosition,
        final int itemPosition,
        boolean isLastChild,
        View convertView,
        ViewGroup parent
    ) {
        PlanItem item = (PlanItem) getChild(groupPosition, itemPosition);

        convertView = mInflater
            .inflate(R.layout.plan_listing_item, null);

        TextView childItem = (TextView)convertView
            .findViewById(R.id.plan_listing_item);
        childItem.setText(item.getLabel());

        return convertView;
    }

    @Override
    public int getChildrenCount(int listPosition) {
        return plan.offsetGet(listPosition).size();
    }

    @Override
    public Object getGroup(int listPosition) {
        return plan.getKeyByPosition(listPosition);
    }

    @Override
    public int getGroupCount() {
        return plan.getSize();
    }

    @Override
    public long getGroupId(int listPosition) {
        return listPosition;
    }

    @Override
    public int getGroupType(int position) {
        String listTitle = (String) getGroup(position);
        String dayInWeek;

        try {
            SimpleDateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd");
            java.util.Date date = dateFormatter.parse(listTitle);
            dateFormatter.applyPattern("E");

            dayInWeek = dateFormatter.format(date);
        } catch (ParseException e) {
            return 0;
        }

        return dayInWeek.equals("Sat") || dayInWeek.equals("Sun") ? 1 : 0;
    }

    @Override
    public View getGroupView(
        int listPosition,
        boolean isExpanded,
        View convertView,
        ViewGroup parent
    ) {
        ViewHolder holder;
        String listTitle = DateHelper
            .getHumanReadableDate((String)getGroup(listPosition));

        if (convertView == null) {
            holder = new ViewHolder();

            int layoutId = R.layout.plan_listing_group;
            int textViewId = R.id.plan_listing_group;

            if (getGroupType(listPosition) == 1) {
                layoutId = R.layout.plan_listing_group_second;
                textViewId = R.id.plan_listing_group_weekend;
            }

            convertView = mInflater
                .inflate(layoutId, null);
            holder.listTitle = (TextView)convertView
                .findViewById(textViewId);

            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        holder.listTitle.setText(listTitle);

        return convertView;
    }

    private static class ViewHolder {
        TextView listTitle;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public boolean isChildSelectable(int listPosition, int expandedListPosition) {
        return true;
    }
}
