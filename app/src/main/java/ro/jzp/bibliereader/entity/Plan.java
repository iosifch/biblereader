package ro.jzp.bibliereader.entity;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;

public class Plan {
    private LinkedHashMap<String, List<PlanItem>> collection = new LinkedHashMap<>();

    private List<String> keyset;

    public Plan add(String key, PlanItem planItem) {
        if (collection.get(key) == null) {
            collection.put(key, new ArrayList<PlanItem>());
        }

        collection.get(key).add(planItem);

        return this;
    }

    public List<PlanItem> get(String key) {
        return collection.get(key);
    }

    public List<PlanItem> offsetGet(Integer position) {
        if (keyset == null) {
            keyset = new ArrayList<>(collection.keySet());
        }

        return collection.get(keyset.get(position));
    }

    public String getKeyByPosition(Integer position) {
        if (keyset == null) {
            keyset = new ArrayList<>(collection.keySet());
        }

        return keyset.get(position);
    }

    public int getSize() {
        return collection.size();
    }

    public int offsetGetByKey(String key) {
        int length = collection.size();

        for (int i = 0; i < length; i++) {
            if (getKeyByPosition(i).equals(key)) {
                return i;
            }
        }

        return 0;
    }

    public int getPositionByCurrentDay() {
        String currentDate = (new SimpleDateFormat("yyyy-MM-dd"))
            .format(new Date());

        return offsetGetByKey(currentDate);
    }
}
