package ro.jzp.bibliereader.entity;

public class PlanItem {
    private String book;

    private String label;

    private String chapter;

    private String bookTranslation;

    public String getBook() {
        return book;
    }

    public PlanItem setBook(String book) {
        this.book = book;

        return this;
    }

    public String getLabel() {
        return label;
    }

    public PlanItem setLabel(String label) {
        this.label = label;

        return this;
    }

    public String getChapter() {
        return chapter;
    }

    public PlanItem setChapter(String chapter) {
        this.chapter = chapter;

        return this;
    }

    public String getBookVersion() {
        return bookTranslation;
    }

    public PlanItem setBookVersion(String bookTranslation) {
        this.bookTranslation = bookTranslation;

        return this;
    }
}
