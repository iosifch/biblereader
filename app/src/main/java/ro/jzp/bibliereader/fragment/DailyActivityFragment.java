package ro.jzp.bibliereader.fragment;

import android.content.Context;
import android.content.Intent;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;
import org.json.JSONException;
import java.io.IOException;
import java.util.List;
import ro.jzp.bibliereader.adapter.CardDailyReferenceListAdapter;
import ro.jzp.bibliereader.adapter.CardDailyViewAdapter;
import ro.jzp.bibliereader.R;
import ro.jzp.bibliereader.activity.PlanListingActivity;
import ro.jzp.bibliereader.entity.Plan;
import ro.jzp.bibliereader.entity.PlanItem;
import ro.jzp.bibliereader.helper.DateHelper;
import ro.jzp.bibliereader.helper.Helper;
import ro.jzp.bibliereader.repository.PlanRepository;

public class DailyActivityFragment extends Fragment {
    public static String CURRENT_POSITION_EXTRA = "ro.jzp.biblereader.current_position";
    private Context context;
    private int currentPosition;

    @Override
    public View onCreateView(
        LayoutInflater inflater,
        ViewGroup container,
        Bundle savedInstanceState
    ) {
        View view = inflater
            .inflate(R.layout.fragment_card_daily, container, false);
        TextView text = (TextView) view.findViewById(R.id.card_daily_title);

        context = getContext();

        Plan plan;
        try {
            plan = PlanRepository.getInstance().fetch();
        } catch (IOException | JSONException e) {
            plan = new Plan();
        }

        currentPosition = getArguments()
            .getInt(CardDailyViewAdapter.CURRENT_POSITION);

        String date = plan.getKeyByPosition(currentPosition);

        text.setText(DateHelper.getHumanReadableDate(date));

        ListView listView = (ListView) view.findViewById(R.id.card_daily_list);
        listView.setAdapter(new CardDailyReferenceListAdapter(
            context, (List) plan.offsetGet(currentPosition)
        ));
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(
                AdapterView<?> parent,
                View view,
                int position,
                long id
            ) {
                PlanItem planItem = (PlanItem) parent.getItemAtPosition(position);
                startActivity(Helper.generateBibleURLIntent(planItem, getActivity()));
            }
        });

//        ImageButton shareButton = (ImageButton) view.findViewById(R.id.share_button);
//        shareButton.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Intent intent = new Intent();
//                intent.setAction(Intent.ACTION_SEND);
//                intent.putExtra(Intent.EXTRA_TEXT, "This is a nice text");
//                intent.setType("text/plain");
//                startActivity(Intent.createChooser(intent, ""));
//            }
//        });

        TextView viewAllList = (TextView) view.findViewById(R.id.view_all_list);
        viewAllList.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, PlanListingActivity.class);
                intent.putExtra(
                    DailyActivityFragment.CURRENT_POSITION_EXTRA,
                    currentPosition
                );
                startActivity(intent);
            }
        });

        return view;
    }
}
