package ro.jzp.bibliereader.fragment;

import android.os.Bundle;
import android.support.v7.preference.ListPreference;
import android.support.v7.preference.Preference;
import android.support.v7.preference.PreferenceFragmentCompat;
import android.support.v7.preference.Preference.OnPreferenceChangeListener;

import ro.jzp.bibliereader.R;
import ro.jzp.bibliereader.helper.Helper;

public class SettingsFragment extends PreferenceFragmentCompat {
    ListPreference translationsListPreference;

    @Override
    public void onCreatePreferences(Bundle bundle, String s) {
        addPreferencesFromResource(R.xml.settings);



        translationsListPreference = (ListPreference) findPreference("bible_version_preference");
        translationsListPreference.setSummary(
            Helper.getBibleVersionEntryPreference(
                getContext(),
                translationsListPreference.getValue()
            )
        );

        translationsListPreference
            .setOnPreferenceChangeListener(new OnPreferenceChangeListener() {
                @Override
                public boolean onPreferenceChange(Preference preference, Object o) {
                    translationsListPreference.setSummary(Helper.getBibleVersionEntryPreference(
                        getContext(),
                        (String) o
                    ));

                    return true;
                }
            });
    }
}
