package ro.jzp.bibliereader.helper;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;

public class DateHelper {
    final private static String[] romDays = {"Duminică", "Luni", "Marți", "Miercuri", "Joi", "Vineri", "Sâmbătă"};
    final private static String[] romMonths = {"Ianuarie", "Februarie", "Martie", "Aprilie", "Mai", "Iunie", "Iulie", "August", "Septembrie", "Octombrie", "Noiembrie", "Decembrie"};

    public static String getHumanReadableDate(String date) {
        try {
            Calendar calendar = Calendar.getInstance();
            calendar.setTime((new SimpleDateFormat("yyyy-MM-dd")).parse(date));
            date = romDays[calendar.get(Calendar.DAY_OF_WEEK) - 1] + ", "
                + calendar.get(Calendar.DAY_OF_MONTH) + " "
                + romMonths[calendar.get(Calendar.MONTH)];
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return date;
    }
}
