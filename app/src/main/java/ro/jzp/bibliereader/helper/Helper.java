package ro.jzp.bibliereader.helper;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.support.v7.preference.PreferenceManager;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;

import ro.jzp.bibliereader.R;
import ro.jzp.bibliereader.entity.PlanItem;

public class Helper {
    public static String fetchDataFromUrl(URL url) throws IOException {
        BufferedReader in = new BufferedReader(
            new InputStreamReader(url.openStream())
        );
        String input;
        String data = "";

        while ((input = in.readLine()) != null) {
            data += input;
        }

        return data;
    }

    public static Intent generateBibleURLIntent(PlanItem planItem, Context context) {
        SharedPreferences settings = PreferenceManager
            .getDefaultSharedPreferences(context);
        String translationIndex = settings
            .getString("bible_version_preference", "126");

        return new Intent(Intent.ACTION_VIEW, Uri.parse(
            "https://www.bible.com/bible/"
                + translationIndex + "/"
                + planItem.getBook()
                + "." + planItem.getChapter()
        ));
    }

    public static String getBibleVersionEntryPreference(
        Context context,
        String versionValue
    ) {
        String[] versionIndexes = context
            .getResources().getStringArray(R.array.bible_versions_index);
        String[] versionEntries = context
            .getResources().getStringArray(R.array.bible_versions);

        int versionIndex = 0;
        for (int i = 0; i < versionIndexes.length; i++) {
            if (versionIndexes[i].equals(versionValue)) {
                versionIndex = i;
                break;
            }
        }

        return versionEntries[versionIndex];
    }
}
