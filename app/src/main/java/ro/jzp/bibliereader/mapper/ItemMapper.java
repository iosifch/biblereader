package ro.jzp.bibliereader.mapper;

import org.json.JSONException;
import org.json.JSONObject;
import ro.jzp.bibliereader.entity.PlanItem;

public class ItemMapper {
    private static final String BOOK = "book";
    private static final String BOOK_VERSION = "version";
    private static final String CHAPTER = "chapter";
    private static final String LABEL = "label";

    public PlanItem fromJson(JSONObject jsonItem) throws JSONException {
        PlanItem planItem = new PlanItem();

        planItem.setBook(jsonItem.getString(BOOK))
            .setBookVersion(jsonItem.getString(BOOK_VERSION))
            .setChapter(jsonItem.getString(CHAPTER))
            .setLabel(jsonItem.getString(LABEL));

        return planItem;
    }
}
