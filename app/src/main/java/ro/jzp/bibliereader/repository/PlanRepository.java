package ro.jzp.bibliereader.repository;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.net.URL;

import ro.jzp.bibliereader.entity.Plan;
import ro.jzp.bibliereader.helper.Helper;
import ro.jzp.bibliereader.mapper.ItemMapper;

public class PlanRepository {
    private final String sourceUrl = "https://api.github.com/gists/5fa0d905ccbb6cbb817eb95fba525d27";
    private final String planFilename = "plan_2017.min.json";
    private ItemMapper itemMapper = new ItemMapper();
    private static Plan plan;
    private static PlanRepository instance;

    private PlanRepository() {}

    public static PlanRepository getInstance() {
        if (null == instance) {
            instance = new PlanRepository();
        }

        return instance;
    }

    public Plan fetch() throws IOException, JSONException {
        if (null != plan) {
            return plan;
        }

        JSONObject sourceRoot = new JSONObject(
            Helper.fetchDataFromUrl(new URL(sourceUrl))
        );

        String sourcePlanUrl = sourceRoot
            .getJSONObject("files")
            .getJSONObject(planFilename)
            .getString("raw_url");

        JSONArray jsonPlan = new JSONArray(
            Helper.fetchDataFromUrl(new URL(sourcePlanUrl))
        );

        plan = createPlanList(jsonPlan);

        return plan;
    }

    private Plan createPlanList(JSONArray jsonPlan) throws JSONException {
        int planSize = jsonPlan.length();
        Plan plan = new Plan();

        for (int i = 0; i < planSize; i++) {
            JSONArray dayItems = jsonPlan.getJSONObject(i).getJSONArray("items");
            int dayItemsSize = dayItems.length();

            for (int j = 0; j < dayItemsSize; j++) {
                plan.add(
                    jsonPlan.getJSONObject(i).getString("date"),
                    itemMapper.fromJson(dayItems.getJSONObject(j))
                );
            }
        }

        return plan;
    }
}
